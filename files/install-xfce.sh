#!/bin/bash

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


echo "### Installing Xfce Desktop"

apt-get --yes install xfce4 xfce4-goodies vim-gtk3 chromium-browser
