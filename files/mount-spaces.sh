#!/bin/bash

#set -o xtrace
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


echo "### Mounting Spaces"

readonly ACCESS_KEY=${1}; shift
readonly SECRET_KEY=${1}; shift
readonly SPACES_NAME=${1}; shift
readonly SPACES_ENDPOINT=${1}; shift

readonly USER_ID=$(id --user ubuntu)
readonly GROUP_ID=$(id --group ubuntu)

apt-get --yes install s3fs

echo ${ACCESS_KEY}:${SECRET_KEY} > /root/.passwd-s3fs
chmod 600 /root/.passwd-s3fs

mkdir --parents /home/ubuntu/Spaces
chown ubuntu:ubuntu /home/ubuntu/Spaces

echo "s3fs#${SPACES_NAME}    /home/ubuntu/Spaces   fuse    url=https://${SPACES_ENDPOINT},allow_other,uid=${USER_ID},gid=${GROUP_ID},mp_umask=007,_netdev,nosuid,nodev    0 0" >> /etc/fstab
