#!/bin/bash

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


echo "### Installing TigerVNC server"

apt-get --yes install tigervnc-standalone-server

# Configure VNC
## Systemd user service unit
cp ./vncserver@.service /usr/lib/systemd/user/vncserver@.service

## User configuration
mkdir -p /home/ubuntu/.vnc
cp ./vnc.conf /home/ubuntu/.vnc

## Keep VNC server running when SSH disconnects
loginctl enable-linger ubuntu
## Enable Systemd unit for an user (systemctl --user enable vncserver@:1.service)
mkdir -p /home/ubuntu/.config/systemd/user/default.target.wants/
ln -svf /usr/lib/systemd/user/vncserver@.service /home/ubuntu/.config/systemd/user/default.target.wants/vncserver@:1.service

# Change ownership to the ubuntu user
chown -Rf ubuntu:ubuntu /home/ubuntu/* || true
chown -Rf ubuntu:ubuntu /home/ubuntu/.* || true
