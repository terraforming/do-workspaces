output "ssh_cmd" {
  value = "ssh -i ${module.workspace.ssh_key} -p 22 -L 5901:localhost:5901 -N ubuntu@${module.workspace.public_ip}"
}
