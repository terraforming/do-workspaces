# Linux Desktop in DigitalOcean

Xfce Desktop running in DigitalOcean. Kind of [AWS Workspaces](https://aws.amazon.com/workspaces/) alternative :)

## Features

* Ubuntu 18.04 (based on [terraform-do-docker-on-beaver](https://bitbucket.org/terraforming/terraform-do-docker-on-beaver))
* [TigerVNC](http://tigervnc.org/) Server
* No VNC authentication (using SSH port-forwarding)
* **Single user**
* Spaces mounted ([s3fs](https://github.com/s3fs-fuse/s3fs-fuse)) in user home directory for easy file synchronization

## Requirements

* VNC Viewer (e.g. ```vncviewer64-x.y.z.exe``` from [TigerVNC](https://bintray.com/tigervnc/stable/tigervnc/) is fine)
* [GitBash](https://gitforwindows.org/) (on Windows)

## Deployment

* Create DigitalOcean [account](https://m.do.co/c/cb8c6d8114de) (referral link)

* Generate [API token](https://www.digitalocean.com/docs/api/create-personal-access-token/)

* Create [Spaces](https://www.digitalocean.com/docs/spaces/how-to/create-and-delete/)

* Note the Spaces endpoint  
  In Spaces listing, select _More_ (on right from Spaces name) and _Settings_ from dropdown

* Generate Spaces [access key](https://www.digitalocean.com/docs/spaces/how-to/administrative-access/)  
  DO Spaces API is AWS S3 compatible. access_key and secret_key are _AWS credentials_ to access your Spaces.
  
* Clone the [do-workspaces](https://bitbucket.org/terraforming/do-workspaces) repository

        git clone https://bitbucket.org/terraforming/do-workspaces.git
        cd do-workspaces

* Create ```terraform.tfvars``` with the following content

        do_token          = "API_TOKEN"
        spaces_access_key = "SPACES_ACCESS_KEY"
        spaces_secret_key = "SPACES_SECRET_KEY"
        spaces_name       = "SPACES_NAME"
        spaces_endpoint   = "SPACES_ENDPOINT"

* Deploy with Terraform

        terraform init
        terraform apply

## Usage

1. Forward local port ```5901``` to Droplet with SSH command from Terraform output

        $(terraform output ssh_cmd)
        # Above will execute something like
        # ssh -i .ssh/id_rsa-do-12345678 -p 22 -L 5901:localhost:5901 -N ubuntu@1.2.3.4

2. Start VNC Viewer and connect to ```localhost:5901```

## Tips

* Just close the VNC Viewer window if you logout (from Xfce session) the VNC server will shutdown and you would need to restart server with

        systemctl --user restart vncserver@:1.service

* Create Droplet and mounted Spaces in the same DigitalOcean region for a better performance (check Spaces [regional availability](https://www.digitalocean.com/docs/spaces/overview/#regional-availability))

* For a better security create a [Firewall](https://www.digitalocean.com/docs/networking/firewalls/how-to/create/). Just a default rules are fine when the VNC port (```5901```) is forwarded through SSH.

* If you have a registered domain (with [DigitalOcean's nameservers](https://www.digitalocean.com/community/tutorials/how-to-point-to-digitalocean-nameservers-from-common-domain-registrars)) you can make a record either manually or with Terraform [digitalocean_record](https://www.terraform.io/docs/providers/do/r/record.html) to get better Workspace Droplet name

* With [doctl](https://github.com/digitalocean/doctl) you may control Workspace Droplet from command line

        # Stop Droplet
        doctl compute droplet-action shutdown DROPLET_ID
        # Start Droplet
        doctl compute droplet-action power-on DROPLET_ID

### Access Spaces with AWS CLI

Install [AWS CLI](https://aws.amazon.com/cli/) and configure the profile ```spaces```

        aws configure --profile spaces set aws_access_key_id SPACES_ACCESS_KEY
        aws configure --profile spaces set aws_secret_access_key SPACES_SECRET_KEY

Create a Bash alias for the ```aws s3``` command

        alias spaces='aws --profile spaces --endpoint-url https://SPACES_ENDPOINT s3'

All AWS CLI [S3 commands](https://docs.aws.amazon.com/cli/latest/reference/s3/index.html#available-commands) are supported. E.g.

        spaces ls s3://SPACES_NAME
        spaces cp ~/Pictures/i_am_ugly.png s3://SPACES_NAME/Pictures/
        spaces rm s3://SPACES_NAME/Desktop/secret.txt
        spaces sync ~/Documents s3://SPACES_NAME/Documents
