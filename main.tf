provider "digitalocean" {
  token = "${var.do_token}"
}

resource "random_pet" "deployment" {}

module "deployment" {
  source = "git::https://bitbucket.org/terraforming/terraform-deployment-id.git?ref=master"
}

module "workspace" {
  source        = "git::https://bitbucket.org/terraforming/terraform-do-docker-on-beaver.git?ref=master"
  deployment_id = "${module.deployment.id}"
  hostname      = "workspace-${random_pet.deployment.id}"
  reboot        = "false"
}

resource "null_resource" "setup" {
  provisioner "remote-exec" {
    inline = [
      "mkdir --parents /var/tmp/workspaces",
    ]

    connection {
      host        = "${module.workspace.public_ip}"
      private_key = "${file("${path.module}/${module.workspace.ssh_key}")}"
    }
  }

  provisioner "file" {
    source      = "${path.module}/files/"
    destination = "/var/tmp/workspaces"

    connection {
      host        = "${module.workspace.public_ip}"
      private_key = "${file("${path.module}/${module.workspace.ssh_key}")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /var/tmp/workspaces",
      "chmod +x *.sh",
      "./install-xfce.sh",
      "./install-vnc.sh",
      "./mount-spaces.sh '${var.spaces_access_key}' '${var.spaces_secret_key}' '${var.spaces_name}' '${var.spaces_endpoint}'",
      "shutdown --reboot +1",
    ]

    connection {
      host        = "${module.workspace.public_ip}"
      private_key = "${file("${path.module}/${module.workspace.ssh_key}")}"
    }
  }

  depends_on = ["module.workspace"]
}
