variable "do_token" {
  description = "DigitalOcean API Key"
}

variable "spaces_access_key" {}
variable "spaces_secret_key" {}
variable "spaces_name" {}
variable "spaces_endpoint" {}
